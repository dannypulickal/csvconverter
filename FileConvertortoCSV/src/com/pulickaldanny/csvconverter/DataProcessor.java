package com.pulickaldanny.csvconverter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

public class DataProcessor {
	public void processData(List<MetaData> mdl, File inputFile, File outputFile) {
		String errorMessage = "";
		int lineCount = 0;
		File ifile = inputFile;
		File ofile = outputFile;
		FileInputStream inputStream = null;
		FileOutputStream outputStream = null; 
		Scanner sc = null;
		try {
			inputStream = new FileInputStream(ifile);
			sc = new Scanner(inputStream, "UTF-8");
			outputStream = new FileOutputStream(ofile);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(outputStream));
			String headerLine = "";
			int maxLineLength = 0;
			for (MetaData metaData : mdl) {
					if(!"".equals(headerLine))
						headerLine+=",";
					headerLine+=metaData.getColumnName();
					maxLineLength+=metaData.getColumnLength();
			}
			bw.write(headerLine);
			System.out.println("Header Line:"+headerLine);
			bw.newLine();
			while (sc.hasNextLine()) {
				lineCount++;
				String line = sc.nextLine();
				int linelength = line.length();
				if (linelength>maxLineLength) {
					System.out.println("Error: Line "+lineCount+" in inputData file has more character than expected");
					bw.close();
					System.exit(0);
				}
				int beginIndex = 0;
				int endIndex = 0;
				String stringtoWrite = "";
				for (MetaData metaData : mdl) {
					String formattedData = "";
					endIndex = beginIndex+metaData.getColumnLength();
					String inputData = line.substring(beginIndex, endIndex);
					System.out.println("Extracted Data:"+inputData);
					if ("Date".equalsIgnoreCase(metaData.getColumnType())) {
						formattedData = getFormattedDate(inputData);
					}else if("String".equalsIgnoreCase(metaData.getColumnType())) {
						formattedData = getFormattedString(inputData);
					}else if("Numeric".equalsIgnoreCase(metaData.getColumnType())) {
						formattedData = getFormattedNumeric(inputData);
					}
					if(!"".equals(stringtoWrite))
						stringtoWrite+=",";
					stringtoWrite+=formattedData;
					beginIndex = endIndex;
				}
				bw.write(stringtoWrite);
				System.out.println("Line "+lineCount+":"+stringtoWrite);
				bw.newLine();
			}
			bw.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) { 
			e.printStackTrace();
			//TODO add Exception
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (sc != null) {
				sc.close();
			}
		}
	}

	private String getFormattedNumeric(final String inputData) {
		try {
			double number = Double.parseDouble(inputData);
		}catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return inputData.trim();
	}

	private String getFormattedString(final String inputData) {
		return "\""+inputData.trim()+"\"";
	}

	private String getFormattedDate(final String inputData) {
		String input[] = inputData.split("-");
		int year = 0;
		int month = 0;
		int day = 0;
		try {
			year = Integer.parseInt(input[0]);
			month = Integer.parseInt(input[1]);
			day = Integer.parseInt(input[2]);
		}catch (NumberFormatException e) {
			e.printStackTrace();
		}
		
		LocalDate date = LocalDate.of(year, month, day);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		return(date.format(formatter));
		
	}
	
	
}
