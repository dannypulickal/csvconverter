package com.pulickaldanny.csvconverter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MetaDataCollector {
	public List<MetaData> collectMetaDataInfo() {
		String errorMessage = "";
		int lineCount = 0;
		File file = new File("C:\\Users\\Danny\\Desktop\\metaDatafile.txt");
		FileInputStream inputStream = null;
		Scanner sc = null;
		List<MetaData> metaDataList = new ArrayList<>();
		try {
			inputStream = new FileInputStream(file);
			sc = new Scanner(inputStream, "UTF-8");
			while (sc.hasNextLine()) {
				lineCount++;
				String line = sc.nextLine();
				// System.out.println(line);
				String[] information = line.split(",");
				if (information.length != 3) {
					errorMessage += "Number of information provided in the line " + lineCount
							+ " of meta data file does not match requirement";
				}
				System.out.println("Column Type: " + information[information.length - 1]);
				if (!("String".equalsIgnoreCase(information[information.length - 1])
						|| "Date".equalsIgnoreCase(information[information.length - 1])
						|| "Numeric".equalsIgnoreCase(information[information.length - 1]))) {
					errorMessage += "Data Type " + information[information.length - 1] + " provided in the line "
							+ lineCount + " of meta data file is not understandable";
				}
				if ("Date".equalsIgnoreCase(information[information.length - 1]) && !("10".equals(information[1]))) {
					errorMessage += "Length of " + information[information.length - 1] + " data type must be 10";
				}
				if (!"".equals(errorMessage)) {
					System.out.println("Error detected. Error message: " + errorMessage);
					System.exit(0);
				}
				MetaData md = new MetaData();
				md.setColumnName(information[0]);
				md.setColumnLength(Integer.parseInt(information[1]));
				md.setColumnType(information[2]);
				metaDataList.add(md);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {  //TODO: change to Exception
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (sc != null) {
				sc.close();
			}
		}
		return metaDataList;
	}
}
